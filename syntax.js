console.log('Задание 1');

for (var x = 1; x <= 100; x++) {
    if (x % 2 == 0) {
        console.log(x);
    }
}

console.log ('Задание 2');

for (var age = 0; age <= 100; age++) {
    if (age <= 17) {
        console.log('вы слишком молоды');
    }
    if (age >= 18 && age <= 60) {
        console.log('отличный возраст');
    }
    if (age >= 61) {
        console.log('привет, бабуля!');
    }
}


console.log ('Задание 3');

var months = {
    1: 31,
    2: 28,
    3: 31,
    4: 30,
    5: 31,
    6: 30,
    7: 31,
    8: 31,
    9: 30,
    10: 31,
    11: 30,
    12: 31
};

for (var month = 1; month <= 12; month++) {
    for (var day = 1; day <= months[month]; day++) {
        var date = day + '.' + month;
        switch (date) {
            case '8.3':
                console.log('с 8 марта!');
                break;
            case '23.2':
                console.log('с 23 февраля!');
                break;
            case '31.12':
                console.log('с Новым годом!');
                break;
            default:
                console.log('сегодня просто отличный день и безо всякого праздника');
        }
    }
}



